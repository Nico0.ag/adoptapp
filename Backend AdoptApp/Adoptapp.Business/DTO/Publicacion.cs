﻿using System;

namespace Adoptapp.Business.DTO
{
    public class Publicacion
    {
        public int IdPublicacion { get; set; }    
        public DateTime FechaPublicacion { get; set; }
        public string Telefono { get; set; }    
        public string Correo { get; set; }  
        public string Fotos { get; set; }   
        public int Localidad { get; set; }  
        public string NombrePublicador { get; set; }

    }
}
