﻿namespace Adoptapp.Business.DTO
{
    public class Localidades
    {
        public int IdLocalidad { get; set; }
        public string NombreLocalidad { get; set; } 
    }
}
