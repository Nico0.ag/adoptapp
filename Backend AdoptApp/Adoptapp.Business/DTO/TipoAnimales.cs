﻿namespace Adoptapp.Business.DTO
{
    public class TipoAnimales
    {
        public int IdAnimal { get; set; }
        public string Descripcion { get; set; } 
    }
}
